package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcManager {

    private JdbcManager(){

    }

    private static final String CONNECTION_URL =
            "jdbc:postgresql://" +
                    "ec2-52-45-183-77.compute-1.amazonaws.com" +
                    "/daq407s82ora5?" +
                    "user=jfpjvtclvwhqla" +
                    "&password=2202d847a225e2c7d723d7be046f66f14de9e75da5176bb9e874691f43db2cb8";

    static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(CONNECTION_URL);
    }

    static User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }

    static Team extractTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }
}
