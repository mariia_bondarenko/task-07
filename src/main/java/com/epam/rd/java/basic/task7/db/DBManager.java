package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

import static com.epam.rd.java.basic.task7.db.Constants.*;


public class DBManager {

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			con = JdbcManager.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_USERS);
			while (rs.next()) {
				users.add(JdbcManager.extractUser(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot find all users from the database",e);
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(stmt);
			DBUtils.close(con);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		boolean result = false;
		if (getUser(user.getLogin()) != null){
			return false;
		}
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = JdbcManager.getConnection();
			pstmt = con.prepareStatement(
					SQL_CREATE_USER,
					Statement.RETURN_GENERATED_KEYS);

			pstmt.setString(1, user.getLogin());

			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					user.setId(rs.getInt(1));
					result = true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot insert a new user to database",e);
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(pstmt);
			DBUtils.close(con);
		}
		return result;
	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean result = false;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = JdbcManager.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(
					Connection.TRANSACTION_READ_COMMITTED);

			pstmt = con.prepareStatement(
					SQL_DELETE_USER,
					Statement.RETURN_GENERATED_KEYS);

			for (User user : users) {
				pstmt.setInt(1, user.getId());
				pstmt.executeUpdate();
				result = true;
			}

			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtils.rollback(con);
			throw new DBException("Cannot insert users: " + Arrays.toString(users), e);
		} finally {
			DBUtils.closeStatement(pstmt);
			DBUtils.close(con);
		}
		return result;
	}

	public User getUser(String login) throws DBException {
		User user = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = JdbcManager.getConnection();
			pstmt = con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
			pstmt.setString(1, login);

			rs = pstmt.executeQuery();

			if (rs.next()) {
				user = JdbcManager.extractUser(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot find a user with login - "+ login,e);
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(pstmt);
			DBUtils.close(con);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = JdbcManager.getConnection();
			pstmt = con.prepareStatement(SQL_FIND_TEAM_BY_NAME);
			pstmt.setString(1, name);

			rs = pstmt.executeQuery();

			if (rs.next()) {
				team = JdbcManager.extractTeam(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot find a team with name - " + name,e);
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(pstmt);
			DBUtils.close(con);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		Statement stmt;
		ResultSet rs;

		try {
			con = JdbcManager.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS);
			while (rs.next()) {
				teams.add(JdbcManager.extractTeam(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot find any team on the database",e);
		} finally {
			DBUtils.close(con);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		boolean result = false;
		if (getUser(team.getName()) != null){
			return false;
		}
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = JdbcManager.getConnection();
			pstmt = con.prepareStatement(
					SQL_CREATE_TEAM,
					Statement.RETURN_GENERATED_KEYS);

			int k = 1;
			pstmt.setString(k++, team.getName());

			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					team.setId(rs.getInt(1));
					result = true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot insert a new team to database",e);
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(pstmt);
			DBUtils.close(con);
		}
		return result;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		boolean result = false;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = JdbcManager.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(
					Connection.TRANSACTION_READ_COMMITTED);

			pstmt = con.prepareStatement(SQL_CREATE_USER_TEAMS);
			User user1 = getUser(user.getLogin());
			for (Team team : teams) {
				pstmt.setInt(1, user1.getId());
				pstmt.setInt(2, team.getId());
				pstmt.executeUpdate();
				result = true;
			}

			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtils.rollback(con);
			throw new DBException("Cannot add user" + user.getLogin() + " to teams: " + Arrays.toString(teams), e);
		} finally {
			DBUtils.closeStatement(pstmt);
			DBUtils.close(con);
		}

		return result;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = JdbcManager.getConnection();
			con.setAutoCommit(false);
			con.setTransactionIsolation(
					Connection.TRANSACTION_READ_COMMITTED);
			pstmt = con.prepareStatement(
					SQL_FIND_USER_TEAMS);

			int k = 1;
			pstmt.setString(k++, user.getLogin());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				teams.add(JdbcManager.extractTeam(rs));
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			DBUtils.rollback(con);
			throw new DBException("Cannot get teams of user - " + user.getLogin(),e);
		} finally {
			DBUtils.closeResultSet(rs);
			DBUtils.closeStatement(pstmt);
			DBUtils.close(con);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		int result;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = JdbcManager.getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_TEAM);
			int k = 1;
			pstmt.setString(k++, team.getName());
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot delete the team" + team.getName(),e);
		} finally {
			DBUtils.closeStatement(pstmt);
			DBUtils.close(con);
		}
		return result != 0;
	}

	public boolean updateTeam(Team team) throws DBException {
		int result;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = JdbcManager.getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_TEAM);

			int k = 1;
			pstmt.setString(k++, team.getName());
			pstmt.setInt(k++, team.getId());
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Cannot update the team" + team.getName(),e);
		} finally {
			DBUtils.closeStatement(pstmt);
			DBUtils.close(con);
		}
		return result != 0;
	}

}
