package com.epam.rd.java.basic.task7.db;

public class Constants {

    private Constants() {
    }

    static final String SQL_FIND_ALL_USERS =
            "select * from users";
    static final String SQL_FIND_ALL_TEAMS =
            "select * from teams";
    static final String SQL_FIND_USER_BY_LOGIN =
            "select * from users where login=?";
    static final String SQL_FIND_TEAM_BY_NAME =
            "select * from teams where name=?";
    static final String SQL_FIND_USER_TEAMS =
            "select * from teams t, users_teams ut, users u where t.id=ut.team_id AND ut.user_id=u.id AND u.login=?";

    static final String SQL_CREATE_USER =
            "insert into users values (default, ?)";
    static final String SQL_CREATE_TEAM =
            "insert into teams values (default, ?)";
    static final String SQL_CREATE_USER_TEAMS =
            "insert into users_teams values (?, ?)";


    static final String SQL_UPDATE_TEAM =
            "update teams set name=? where id=?";

    static final String SQL_DELETE_TEAM =
            "delete from teams where name=?";
    static final String SQL_DELETE_USER =
            "delete from users where id=?";
    }
