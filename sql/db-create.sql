DROP database IF EXISTS task7db;

CREATE database task7db;

CREATE SEQUENCE s_user;
CREATE TABLE users (id INT PRIMARY KEY DEFAULT NEXTVAL('s_user'),
	login VARCHAR(10) UNIQUE);

CREATE SEQUENCE s_team;
CREATE TABLE teams (id INT PRIMARY KEY DEFAULT NEXTVAL('s_team'),
	name VARCHAR(10));

CREATE TABLE users_teams (
	user_id INT REFERENCES users(id) on delete cascade,
	team_id INT REFERENCES teams(id) on delete cascade,
	UNIQUE (user_id, team_id));


INSERT INTO users VALUES (NEXTVAL('s_user'), 'ivanov');
INSERT INTO teams VALUES (NEXTVAL('s_team'), 'teamA');

SELECT * FROM users;
SELECT * FROM teams;
SELECT * FROM users_teams;

